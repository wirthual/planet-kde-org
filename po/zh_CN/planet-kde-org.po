msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2024-04-07 00:34\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/websites-planet-kde-org/planet-kde-org."
"pot\n"
"X-Crowdin-File-ID: 25368\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "KDE 星球"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "KDE 星球网站为您提供来自 KDE 项目的最新资讯"

#: config.yaml:0
msgid "Add your own feed"
msgstr "添加您的订阅源"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "欢迎来到 KDE 星球"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"KDE 星球是一个资讯源聚合网站，它集合了 [KDE 社区](https://kde.org) 的贡献者在"
"自己的博客上用不同的语言撰写的文章。"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "转到下一篇"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "转到上一篇"
