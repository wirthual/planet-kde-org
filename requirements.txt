# SPDX-FileCopyrightText: 2023 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: CC0-1.0

PyYAML
feedparser
hugo-gettext
